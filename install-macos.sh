#!/bin/bash
GIT_ZSH_FOLDER="./zsh"
GIT_VIM_FOLDER="./vim"

function install_homebrew(){
    which -s brew
    if [[ $? != 0 ]] ; then
        /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    fi
}

platform=unknown
unamestr=`uname`

if [[ unamestr == "Darwin" ]] ; then
    unamestr=macos
elif [[ unamestr == "Linux" ]] ; then
    unamestr=linux
fi

#install homebrew
if [[ unamestr == macos ]] ; then
    echo "Checking if homebrew is installed"
    install_homebrew
    echo "Installing zsh"
    brew install zsh
elif [[ unamestr == linux ]] ; then
    echo "Checking if homebrew is installed"
    which brew
    if [[ $? != 0 ]] ; then
        sh -c "$(curl -fsSL https://raw.githubusercontent.com/Linuxbrew/install/master/install.sh)"
        brew install zsh
    fi
fi

git clone https://github.com/tarjoilija/zgen.git "${HOME}/.zgen"

cp "$GIT_ZSH_FOLDER"/.zshrc ~/.zshrc

chsh -s `which zsh` $(whoami)

echo "Installing vundle"
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
cp ~/.vimrc ~/.vimrc-backup-"$(date +%s)"
cp "$GIT_VIM_FOLDER"/vimrc ~/.vimrc
vim +PluginInstall +qall
