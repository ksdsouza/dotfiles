#!/bin/bash
GIT_ZSH_FOLDER="./zsh"
GIT_VIM_FOLDER="./vim"

echo "Installing zsh"
sudo apt install zsh

sudo apt install vim

git clone https://github.com/tarjoilija/zgen.git "${HOME}/.zgen"

cp "$GIT_ZSH_FOLDER"/.zshrc ~/.zshrc

chsh -s `which zsh` $(whoami) 

echo "Installing vundle"
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
cp ~/.vimrc ~/.vimrc-backup-"$(date +%s)"
cp "$GIT_VIM_FOLDER"/vimrc ~/.vimrc
vim +PluginInstall +qall
